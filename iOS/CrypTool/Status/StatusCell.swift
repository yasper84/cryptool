//
//  StatusCell.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import TableViewViewModel

final class StatusCell: UITableViewCell, ConfigurableTableViewCell {
    
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var actionLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var valueLabel: UILabel!
    
    func configure(with iteration: Iteration) {
        if let timestamp = Double(iteration.timestamp) {
            timeLabel.text = Date(timeIntervalSince1970: timestamp).stringRepresentation
        }
        
        guard let metaInfoSection = iteration.metaInfoSection else { return }
        timeLabel.text = String(format: "%@ (iteration %@)",
                                timeLabel.text ?? "",
                                metaInfoSection.Iteration)
        
        // If we have an order, we have all info
        if let orderSection = iteration.orderSection {
            guard
                let quantity = Float(orderSection.Quantity),
                let price = Float(orderSection.Price) else { return }
            
            priceLabel.text = String(format: "%@ %@",
                                     orderSection.Price,
                                     orderSection.Type)

            valueLabel.text = String(format: "€ %.2f (%.0f)",
                                     quantity * price,
                                     quantity)
            
            contentView.backgroundColor = orderSection.isBuy ? UIColor(red: 0, green: 150, blue: 0) : UIColor.red
            var actionText: String = orderSection.isBuy ? "BUY" : "SELL"
            
            if let sequentialChangeSection = iteration.sequentialChangeSection {
                actionText = String(format: "%@ (%@)",
                                    actionText,
                                    sequentialChangeSection.Message)
            }
            actionLabel.text = actionText
        } else if let stateInfoSection = iteration.stateInfoSection {
            // Else fill in all we can
            contentView.backgroundColor = UIColor.blue
            priceLabel.text = String(format: "%@ %@",
                                     stateInfoSection.NewPrice,
                                     stateInfoSection.CurrentState)
            
            if let sequentialChangeSection = iteration.sequentialChangeSection {
                actionLabel.text = String(format: "IDLE (%@)",
                                          sequentialChangeSection.Message)
            }
            
            valueLabel.text = "*"
        }
    }
}
