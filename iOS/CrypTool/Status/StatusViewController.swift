//
//  ViewController.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 30/06/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import TableViewViewModel

final class StatusViewController: CryptoViewController {
    
    private lazy var viewModel = StatusViewModel(statusDelegate: self)

    override func viewDidLoad() {
        super.viewDidLoad()
        
        let refreshButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.refresh,
                                            target: self,
                                            action: #selector(onRefreshPressed))
        navigationItem.rightBarButtonItem = refreshButton
    }
    
    @objc private func onRefreshPressed() {
        viewModel.retrieveStatus()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        viewModel.retrieveStatus()
    }
}

extension StatusViewController: StatusDelegate {
    
    func onStatusUpdated() {
        tableView.isHidden = false
        statusLabel.isHidden = true
        
        let sectionViewController = TableViewSectionViewModel(viewModels: viewModel.iterations,
                                                              cellType: StatusCell.self,
                                                              cellHeight: 50)
        tableView.viewModel = TableViewViewModel(sections: [sectionViewController])
    }
    
    func onStatusRetrievalFailure(error: NSError) {
        tableView.isHidden = true
        statusLabel.isHidden = false
        
        statusLabel.text = error.debugDescription
    }
}
