//
//  StatusViewModel.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

protocol StatusDelegate: class {
    func onStatusUpdated()
    func onStatusRetrievalFailure(error: NSError)
}

typealias JSONIteration = [String : [[String : Any]]]

final class StatusViewModel: NSObject {
    
    private struct Constants {
        static let statusMethod: String = "status"
    }
    
    private weak var statusDelegate: StatusDelegate?
    private(set) var iterations: [Iteration] = []

    init(statusDelegate: StatusDelegate) {
        self.statusDelegate = statusDelegate
    }
    
    
    func retrieveStatus() {
        do {
            guard let url = URL(string: String(format: "%@/%@", AppConfig.SERVER_BASE_URL, Constants.statusMethod)) else { return }
            let statusData: Data = try Data(contentsOf: url)
            let statusInfo = try JSONSerialization.jsonObject(with: statusData,
                                                              options: [])
            guard let iterationsInfo = statusInfo as? [[String: [[String: Any]]]] else { return }
            
            iterations.removeAll()
            iterationsInfo.forEach { (JSONIteration) in
                guard
                    let timestamp = JSONIteration.first?.key,
                    let iterationInfoArray = JSONIteration.first?.value as? [[String: [String: String]]] else { return }
                
                iterations.append(Iteration(timestamp: timestamp,
                                            content: iterationInfoArray))
            }
            statusDelegate?.onStatusUpdated()
            
            // Store the current settings
            guard
                let lastIteration = iterations.last,
                let metaInfoSection = lastIteration.metaInfoSection else { return }
            UserDefaults.standard.storeCurrentConfig(metaInfoSection)
        } catch {
            statusDelegate?.onStatusRetrievalFailure(error: (error as NSError))
        }
    }
}
