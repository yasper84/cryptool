//
//  Iteration.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

struct MetaInfoSection: Decodable {
    let Iteration: String
    let WillRun: String
    let Crypto: String
    let MinChangeDelta: String
    let SuccessionsRequired: String
    let UpdateInterval: String
}

struct StateInfoSection: Decodable {
    let CurrentState: String
    let Delta: String
    let LastKnownPrice: String
    let NewPrice: String
}

struct SequentialChangeSection: Decodable {
    let Message: String
}

struct OrderSection: Decodable {
    let `Type`: String
    let Quantity: String
    let Price: String
    
    var isBuy: Bool {
        return Type == "BUY"
    }
    
}

struct Iteration: Equatable {
    private struct Constants {
        static let metaInfoSection: String = "IterationInfo"
        static let stateInfoSection: String = "StateInfo"
        static let sequentialChangeSection: String = "SequentialChange"
        static let orderSection: String = "Order"
    }
    
    
    let timestamp: String
    let content: [[String: [String: String]]]
    
    static func == (lhs: Iteration, rhs: Iteration) -> Bool {
        return lhs.timestamp == rhs.timestamp
    }
    
    var metaInfoSection: MetaInfoSection? {
        return infoSection(sectionKey: Constants.metaInfoSection)
    }
    
    var stateInfoSection: StateInfoSection? {
        return infoSection(sectionKey: Constants.stateInfoSection)
    }

    var sequentialChangeSection: SequentialChangeSection? {
        return infoSection(sectionKey: Constants.sequentialChangeSection)
    }

    var orderSection: OrderSection? {
        return infoSection(sectionKey: Constants.orderSection)
    }
    
    func infoSection<T: Decodable>(sectionKey: String) -> T? {
        guard let infoSection = section(name: sectionKey) else { return nil }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: infoSection,
                                                      options: [])
            let infoSection = try JSONDecoder().decode(T.self,
                                                       from: jsonData)
            return infoSection
        } catch {
            return nil
        }
    }
    
    private func section(name: String) -> [String: String]? {
        for infoSection in content {
            if infoSection.keys.first == name {
                return infoSection[name]
            }
        }
        return nil
    }
}
