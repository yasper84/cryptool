//
//  CryptoViewController.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 07/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import TableViewViewModel

class CryptoViewController: UIViewController {

    let statusLabel: UILabel = UILabel(frame: CGRect.zero)
    let tableView: UITableView = UITableView(frame: CGRect.zero,
                                             style: .grouped)

    override func viewDidLoad() {
        super.viewDidLoad()

        createStatusLabel()
        configureTableView()
    }
    
    private func createStatusLabel() {
        view.addSubview(statusLabel)
        statusLabel.centerInSuperview()
        statusLabel.pinWidth(300)
        statusLabel.numberOfLines = 0
        statusLabel.textAlignment = NSTextAlignment.center
    }
    
    func configureTableView() {
        view.addSubview(tableView)
        tableView.pinToSuperview()
        tableView.separatorStyle = .none
        tableView.tableHeaderView = UIView(frame: CGRect(x: 0, y: 0, width: view.width, height: 1))
    }
}
