//
//  AppConfig.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 07/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import Foundation

final class AppConfig: NSObject {
    static let SERVER_BASE_URL: String = "<ENTER YOUR SERVER URL>"
    static let CLIENT_SECRET: String = "<ENTER YOUR KEY>"
    
    public static let sharedInstance = AppConfig()
}


extension UserDefaults {
    func storeCurrentConfig(_ metaInfo: MetaInfoSection) {
        Threshold = metaInfo.MinChangeDelta
        UpdateInterval = metaInfo.UpdateInterval
        Crypto = metaInfo.Crypto
        Successions = metaInfo.SuccessionsRequired
    }
    
    @objc dynamic var Threshold: String? {
        get {
            return string(forKey: ConfigOptions.threshold.rawValue) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue,
                                      forKey: ConfigOptions.threshold.rawValue)
            
        }
    }
    
    @objc dynamic var UpdateInterval: String? {
        get {
            return string(forKey: ConfigOptions.updateInterval.rawValue) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue,
                                      forKey: ConfigOptions.updateInterval.rawValue)
            
        }
    }
    
    @objc dynamic var Crypto: String? {
        get {
            return string(forKey: ConfigOptions.crypto.rawValue) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue,
                                      forKey: ConfigOptions.crypto.rawValue)
            
        }
    }
    
    @objc dynamic var Successions: String? {
        get {
            return string(forKey: ConfigOptions.successions.rawValue) ?? ""
        }
        set {
            UserDefaults.standard.set(newValue,
                                      forKey: ConfigOptions.successions.rawValue)
            
        }
    }
}

