//
//  SettingsEntryViewModel.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 07/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

enum ConfigOptions: String, CaseIterable {
    case threshold = "Threshold"
    case updateInterval = "UpdateInterval"
    case crypto = "Crypto"
    case successions = "Successions"
    case terminate = "Terminate"
}

final class SettingsEntryViewModel: NSObject {
    let option: ConfigOptions
    let inputType: UIKeyboardType?
    var enteredValue: String
    
    init(option: ConfigOptions,
         inputType: UIKeyboardType? = nil) {
        self.option = option
        self.inputType = inputType
        
        enteredValue = UserDefaults.standard.string(forKey: option.rawValue) ?? ""
    }
    
    static func == (lhs: SettingsEntryViewModel, rhs: SettingsEntryViewModel) -> Bool {
        return lhs.option == rhs.option
    }
}
