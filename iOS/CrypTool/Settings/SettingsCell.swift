//
//  SettingsCell.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 07/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import TableViewViewModel

final class SettingsCell: UITableViewCell, ConfigurableTableViewCell {
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var inputTextfield: UITextField!
    
    private var viewModel: SettingsEntryViewModel?
    private var observation: NSKeyValueObservation? {
        willSet {
            observation?.invalidate()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        inputTextfield.addTarget(self,
                                 action: #selector(textFieldDidChange),
                                 for: .editingChanged)
    }
    
    func configure(with model: SettingsEntryViewModel) {
        self.viewModel = model
        
        titleLabel.text = model.option.rawValue
        if let inputType = model.inputType {
            inputTextfield.isHidden = false
            inputTextfield.keyboardType = inputType
            titleLabel.textAlignment = .left
        } else {
            inputTextfield.isHidden = true
            titleLabel.textAlignment = .center
        }
        
        // Auto-update textfield with backend value if it does not have focus
        let keyPath: KeyPath<UserDefaults, String?>?
        switch model.option {
        case ConfigOptions.threshold:
            keyPath = \.Threshold
        case ConfigOptions.crypto:
            keyPath = \.Crypto
        case ConfigOptions.successions:
            keyPath = \.Successions
        case ConfigOptions.updateInterval:
            keyPath = \.UpdateInterval
        default:
            keyPath = nil
        }
        
        guard let definedKeyPath = keyPath else { return }
        observation = UserDefaults.standard.observe(definedKeyPath, options: [.initial, .new]) { [weak self] (_, change) in
            guard let self = self, !self.inputTextfield.isFirstResponder else { return }
            self.inputTextfield.text = change.newValue ?? ""
        }
    }
    
    @objc private func textFieldDidChange() {
        viewModel?.enteredValue = inputTextfield.text ?? ""
    }
    
    deinit {
        observation?.invalidate()
    }
}

extension SettingsCell: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
