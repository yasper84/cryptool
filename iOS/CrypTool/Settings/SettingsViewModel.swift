//
//  SettingsViewModel.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit

protocol ConfigDelegate: class {
    func onSuccess()
    func onError(message: String)
}

final class ConfigViewModel: NSObject {
    
    private struct Constants {
        static let thresholdMethod: String      = "threshold"
        static let updateIntervalMethod: String = "updateInterval"
        static let cryptoMethod: String         = "crypto"
        static let successionsMethod: String    = "successions"
        static let terminateAppMethod: String   = "terminate"
    }
    
    weak var delegate: ConfigDelegate?
    private let dispatchGroup = DispatchGroup()
    
    lazy var configViewModels: [SettingsEntryViewModel] = [SettingsEntryViewModel(option: ConfigOptions.threshold,
                                                                              inputType: UIKeyboardType.decimalPad),
                                                         SettingsEntryViewModel(option: ConfigOptions.updateInterval,
                                                                              inputType: UIKeyboardType.numberPad),
                                                         SettingsEntryViewModel(option: ConfigOptions.crypto,
                                                                              inputType: UIKeyboardType.namePhonePad),
                                                         SettingsEntryViewModel(option: ConfigOptions.successions,
                                                                              inputType: UIKeyboardType.numberPad),
                                                         SettingsEntryViewModel(option: ConfigOptions.terminate)]

    init(delegate: ConfigDelegate){
        self.delegate = delegate
        super.init()
        
        dispatchGroup.notify(queue: .main) {
            self.delegate?.onSuccess()
        }
    }
    
    // Submit entered values to backend
    func storeValues() {
        let values: [String] = configViewModels.map({ return $0.enteredValue })
        var callContent = Array(zip(values,
                                    [Constants.thresholdMethod,
                                     Constants.updateIntervalMethod,
                                     Constants.cryptoMethod,
                                     Constants.successionsMethod]))
        callContent = callContent.filter({ !$0.0.isEmpty })
        
        guard !callContent.isEmpty else {
            delegate?.onSuccess()
            return
        }
        
        callContent.forEach({
            executeCall(method: $0.1,
                        httpType: "PUT",
                        value: $0.0)
        })
    }
    
    
    func terminate() {
        executeCall(method: Constants.terminateAppMethod,
                    httpType: "POST")
    }
    
    private func executeCall(method: String, httpType: String, value: String? = nil) {
        let url: URL?
        if let value = value {
            url = URL(string: String(format: "%@/%@/%@", AppConfig.SERVER_BASE_URL, method, value))
        } else {
            url = URL(string: String(format: "%@/%@", AppConfig.SERVER_BASE_URL, method))
        }
        guard let urlToCall = url else { return }
        
        dispatchGroup.enter()
        
        var request = URLRequest(url: urlToCall)
        request.httpMethod = httpType
        request.addValue(AppConfig.CLIENT_SECRET,
                         forHTTPHeaderField: "secret")
        URLSession.shared.dataTask(with: request,
                                   completionHandler: { [weak self] (data, response, error) in
                                    guard let self = self else { return }
                                    
                                    if let data = data {
                                        print("Response: " + (String(data: data, encoding: String.Encoding.utf8) ?? ""))
                                    }
                                    
                                    DispatchQueue.main.async {
                                        let statusCode = (response as? HTTPURLResponse)?.statusCode ?? 500
                                        if statusCode == 200 {
                                            self.delegate?.onSuccess()
                                        } else if statusCode == 401 {
                                            self.delegate?.onError(message: "Not Authorized")
                                        } else {
                                            let description = (error as NSError?)?.debugDescription ?? "Unknown Error"
                                            self.delegate?.onError(message: description)
                                        }
                                        
                                        self.dispatchGroup.leave()
                                    }
        }).resume()
    }
}
