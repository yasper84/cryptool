//
//  SettingsViewController.swift
//  CrypTool
//
//  Created by Jasper Siebelink on 06/07/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

import UIKit
import TableViewViewModel

final class SettingsViewController: CryptoViewController {
    
    private lazy var viewModel = ConfigViewModel(delegate: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let saveButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.save,
                                            target: self,
                                            action: #selector(onSavePressed))
        navigationItem.rightBarButtonItem = saveButton
    }
    
    override func configureTableView() {
        super.configureTableView()
        
        let sectionViewModel = TableViewSectionViewModel(viewModels: viewModel.configViewModels,
                                                         cellType: SettingsCell.self,
                                                         cellHeight: 50,
                                                         onCellClicked: { [weak self] (viewModel, _) in
                                                            guard
                                                                let self = self,
                                                                viewModel.option == ConfigOptions.terminate else { return }
                                                            self.viewModel.terminate()
        })
        tableView.viewModel = TableViewViewModel(sections: [sectionViewModel])
    }
    
    @objc private func onSavePressed() {
        view.endEditing(true)
        navigationItem.rightBarButtonItem?.isEnabled = false
        viewModel.storeValues()
    }
}

extension SettingsViewController: ConfigDelegate {
    
    func onSuccess() {
        navigationItem.rightBarButtonItem?.isEnabled = true
    }
    
    func onError(message: String) {
        navigationItem.rightBarButtonItem?.isEnabled = true
        tableView.isHidden = true
        statusLabel.isHidden = false
        
        statusLabel.text = message
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.tableView.isHidden = false
            self.statusLabel.isHidden = true
        }
    }
}

