//
//  CommonExtensions.h
//  CommonExtensions
//
//  Created by Jasper Siebelink on 02/03/2019.
//  Copyright © 2019 Jasper Siebelink. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for CommonExtensions.
FOUNDATION_EXPORT double CommonExtensionsVersionNumber;

//! Project version string for CommonExtensions.
FOUNDATION_EXPORT const unsigned char CommonExtensionsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CommonExtensions/PublicHeader.h>


