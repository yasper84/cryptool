from binance.websockets import BinanceSocketManager
from binance.exceptions import BinanceAPIException, BinanceWithdrawException
from binance.client import Client
import CryptoLogger
from enum import Enum
import math

class CryptoState(Enum):
    Ripple = 0
    USDT = 1
    Invalid = 2

class OrderSide(Enum):
    BUY = "BUY"
    SELL = "SELL"

class ExchangeManager:
    # Constants
    __API_KEY = "<ENTER YOUR BINANCE API KEY>"
    __API_SECRET = "<ENTER YOUR BINANCE API SECRET>"
    __ASSET_SYMBOL = "XRPUSDT"

    # Offered orders have to have at least 200 coins to be considered for price calculation
    __MIN_AMOUNT_COINS_BOOK_ENTRY: float = 200

    __client = Client(__API_KEY, __API_SECRET)
    __availableXRP: float
    __availableUSDT: float

    # Swaps all assets to XRP
    # Returns: if a balance is available
    def swapToRipple(self):
        lowestAsk: float = self.getLowestAsk()

        # Format the amount and price to the number decimals Binance requires
        amount = math.floor(self.__availableUSDT / lowestAsk)
        amountStr = "{0:.1f}".format(amount)
        lowestAskStr: str = "{0:.5f}".format(lowestAsk)

        return self.__applyOrder(OrderSide.BUY, amountStr, lowestAskStr)


    # Swaps all assets to USDT
    # Returns: if a balance is available
    def swapToUSDT(self):
        # Format the amount and price to the number decimals Binance requires
        amountStr = "{0:.1f}".format(math.floor(self.__availableXRP))
        highestBidStr = str(self.__getHighestBid())

        return self.__applyOrder(OrderSide.SELL, amountStr, highestBidStr)


    # Returns: if a balance is available
    def verifyBalanceAvailable(self):
        self.__getBalances()

        CryptoLogger.Logger.logDictionary({
            "XRP": str(self.__availableXRP),
            "USDT": str(self.__availableUSDT)
        }, "Balance")

        if self.__availableXRP > 1:
            return CryptoState.Ripple
        elif self.__availableUSDT > 1:
            return CryptoState.USDT
        else:
            return CryptoState.Invalid


    # Order management
    # Returns: Orderbook for XRP/USDT
    def getOrderBook(self):
        return self.__client.get_order_book(symbol=self.__ASSET_SYMBOL)


    # Get and store available balances
    def __getBalances(self):
        self.__availableXRP = self.__getBalanceForAsset('XRP')
        self.__availableUSDT = self.__getBalanceForAsset('USDT')


    # Retrieve the balance for given asset
    # Returns: float of amount available for given asset
    def __getBalanceForAsset(self, asset: str):
        amountStr: float = self.__client.get_asset_balance(asset=asset)["free"]
        return float(amountStr)


    # Either buy or sell XRP/USDT
    # Returns: if balance available
    def __applyOrder(self, side: OrderSide, quantity: str, price: str):
        CryptoLogger.Logger.logDictionary({
            "Type": side.name,
            "Quantity": quantity,
            "Price": price
        }, "Order")
        result = self.__client.create_order(symbol=self.__ASSET_SYMBOL,
                                            side=side.value,
                                            type='LIMIT',
                                            quantity=quantity,
                                            price=price,
                                            timeInForce='FOK')
        return result["status"] == "FILLED"


    # Determine the lowest selling price for XRP
    def getLowestAsk(self):
        # Retrieve all active orders from Binance
        orderBook = self.getOrderBook()

        # Get the active offers
        asks = orderBook["asks"]

        # Filter out too small offers
        if self.__availableXRP > self.__availableUSDT:
            validAsks = list(filter(lambda ask: float(ask[1]) > self.__MIN_AMOUNT_COINS_BOOK_ENTRY, asks))
        else:
            validAsks = list(filter(lambda ask: float(ask[0]) * float(ask[1]) > self.__availableUSDT, asks))

        # Map to a list of prices
        validAskAmounts = list(map(lambda ask: float(ask[0]), validAsks))

        # Determine the lowest value
        return min(validAskAmounts)


    # Determine the highest buying price for XRP
    def __getHighestBid(self):
        # Retrieve all active orders from Binance
        orderBook = self.getOrderBook()

        # Get the active offers
        bids = orderBook["bids"]

        # Filter out any offers with less than the coins we have
        validBids = list(filter(lambda bid: float(bid[1]) > self.__availableXRP, bids))

        # Map to a list of prices
        validBidAmounts = list(map(lambda bid: float(bid[0]), validBids))

        # Determine the highest value
        return max(validBidAmounts)


    # Cancel all open orders (unused since we use 'Fill Or Kill'-orders)
    def cancelOpenOrders(self):
        CryptoLogger.Logger.logDictionary({"Status": "Cancel all orders"}, "Action")

        openOrders = self.__client.get_open_orders()

        for order in openOrders:
            orderID = order["orderId"]
            orderSymbol = order["symbol"]

            self.__client.cancel_order(symbol=orderSymbol,
                                       orderId=orderID)
