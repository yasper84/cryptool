from enum import Enum
import json
import time
import CryptoAnalyzer
import os

CRYPTOOL_LOG = os.path.dirname(os.path.realpath(__file__)) + "/traderLog.json"

class OutputType(Enum):
    typeJSON = "JSON"
    typeDebug = "Debug"

class Logger:
    __CURRENT_OUTPUT_TYPE: OutputType = OutputType.typeJSON
    __iterationDictionaries = []


    @classmethod
    def logDictionary(cls, info: {str: str}, infoKey: str):
        cls.__iterationDictionaries.append({infoKey: info})


    @classmethod
    def logIteration(cls):
        iterationKey = str(round(time.time()))
        infoDictionaryToPrint = {iterationKey: cls.__iterationDictionaries}

        if cls.__CURRENT_OUTPUT_TYPE == OutputType.typeDebug:
          print(json.dumps(
                infoDictionaryToPrint,
                sort_keys=True,
                indent=4))
        else:
            cls.__appendToFile(infoDictionaryToPrint)
        cls.__iterationDictionaries.clear()


    @classmethod
    def skipIteration(cls):
        cls.__iterationDictionaries.clear()


    @classmethod
    def __appendToFile(cls, infoDictionaryToPrint: {str: {str: str}}):
        # Attempt to read the previous log and append
        try:
            readFile = open(CRYPTOOL_LOG, "r")
            previousLog = readFile.read()
            jsonContent = json.loads(previousLog)
            infoDictionaryToPrint = jsonContent.append(infoDictionaryToPrint)
        except:
            jsonContent = [infoDictionaryToPrint]

        # Format to proper JSON
        textToWrite = json.dumps(
            jsonContent,
            sort_keys=True,
            indent=4)

        writeFile = open(CRYPTOOL_LOG, "w+")
        writeFile.write(textToWrite)
        writeFile.close()

    @classmethod
    def inJSONMode(cls):
        return cls.__CURRENT_OUTPUT_TYPE == OutputType.typeJSON
