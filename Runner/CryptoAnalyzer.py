import Exchange
from functools import reduce
from enum import Enum
import CryptoLogger
import time
import CryptoServer


class IterationStatus(Enum):
    idle = "Idle"
    substantialDeltaDetected = "substantialDeltaDetected"
    purchaseMade = "purchaseMade"
    terminated = "Terminated"

class Analyzer:
    # Trading constants
    IDLE_TIME_AFTER_ACTION: int = 20

    def __init__(self):
        # Binance network object
        self.__exchange = Exchange.ExchangeManager()

        # Price calculation variables
        self.__lastKnownPrice = 0
        self.__numSequentialPriceIncreases = 0

        # Current state
        self.__lastTimeActionTaken = 0
        self.__iteration = 0
        self.__exchangeChangeCompleted = True

        self.__cryptoState = self.__exchange.verifyBalanceAvailable()


    # If the price is going down 3x
    #   Convert all Ripple to USDT
    # If the price is going up 3x, buy
    #   Convert all USDT to Ripple
    # Returns false when no balance available, terminating program
    def determineAction(self) -> IterationStatus:
        # If we skip the iteration due to recent activity
        if self.__handleNewIteration() == False and self.__exchangeChangeCompleted == True:
            return IterationStatus.idle

        # If we abort execution due to invalid crypto state
        if self.__cryptoState == Exchange.CryptoState.Invalid:
            return IterationStatus.terminated

        # Determine the average price
        currentPrice = self.__exchange.getLowestAsk()

        # Determine if the price is higher (plus delta) than before
        delta: float = abs(self.__lastKnownPrice - currentPrice)

        CryptoLogger.Logger.logDictionary({
            "CurrentState": self.__cryptoState.name,
            "LastKnownPrice": str(self.__lastKnownPrice),
            "NewPrice": str(currentPrice),
            "Delta": str(delta)
        }, "StateInfo")

        if (self.__lastKnownPrice + CryptoServer.MIN_CHANGE_DELTA) < currentPrice:
            # We increased
            if self.__numSequentialPriceIncreases > 0:
                self.__numSequentialPriceIncreases += 1
            else:
                self.__numSequentialPriceIncreases = 1
        elif(self.__lastKnownPrice - CryptoServer.MIN_CHANGE_DELTA) > currentPrice:
            # We decreased
            if self.__numSequentialPriceIncreases < 0:
                self.__numSequentialPriceIncreases -= 1
            else:
                self.__numSequentialPriceIncreases = -1
        else:
            CryptoLogger.Logger.logDictionary({
                "Message": "No substantial change detected"
            }, "IterationStatus")

            # Always log every 5000th iteration
            if self.__iteration - 1 % 5000 == 0:
                return IterationStatus.purchaseMade
            else:
                return IterationStatus.idle

        CryptoLogger.Logger.logDictionary({
            "Message": str(self.__numSequentialPriceIncreases)
        }, "SequentialChange")

        # Keep track for the next iteration
        self.__lastKnownPrice = currentPrice

        # If we either had a sufficient amount of increases or decreases, take action
        if self.__numSequentialPriceIncreases == CryptoServer.NUM_SUCCESSIONS_REQUIRED:
            self.__changeAssetState(Exchange.CryptoState.Ripple)
            return IterationStatus.purchaseMade
        elif self.__numSequentialPriceIncreases == 0 - CryptoServer.NUM_SUCCESSIONS_REQUIRED:
            self.__changeAssetState(Exchange.CryptoState.USDT)
            return IterationStatus.purchaseMade
        # If we already decided on a previous state, but it has not yet been applied
        elif self.__exchangeChangeCompleted == False:
            self.__commitStateToExchange(self.__cryptoState)
            return IterationStatus.idle
        else:
            # Log first iteration
            if self.__iteration == 1:
                return IterationStatus.purchaseMade
            else:
                return IterationStatus.substantialDeltaDetected


    def __handleNewIteration(self):
        self.__iteration += 1

        shouldRunIteration = False
        if (self.__lastTimeActionTaken + self.IDLE_TIME_AFTER_ACTION) < time.time():
            shouldRunIteration = True

        CryptoLogger.Logger.logDictionary({
            "Iteration": str(self.__iteration),
            "WillRun": str(shouldRunIteration),
            "SuccessionsRequired": str(CryptoServer.NUM_SUCCESSIONS_REQUIRED),
            "MinChangeDelta": str(CryptoServer.MIN_CHANGE_DELTA),
            "UpdateInterval": str(CryptoServer.UPDATE_INTERVAL),
            "Crypto": CryptoServer.CRYPTO
        }, "IterationInfo")

        return shouldRunIteration


    # Change the asset state to the given value locally and attempt to on the exchange as well
    def __changeAssetState(self, newState: Exchange.CryptoState):
        # Update so we don't change future asset state for a while
        self.__lastTimeActionTaken = time.time()

        # Make sure we are not already in this state
        if self.__cryptoState == newState:
            return

        # Update locally to the new state
        self.__cryptoState = newState

        # Attempt to also update remotely
        self.__commitStateToExchange(newState)


    # Attempt to mirror the local state to the exchange
    def __commitStateToExchange(self, state: Exchange.CryptoState):
        # Verify balance available
        currentState = self.__exchange.verifyBalanceAvailable()
        if currentState == Exchange.CryptoState.Invalid:
            return
        else:
            if state == Exchange.CryptoState.Ripple:
                self.__exchangeChangeCompleted = self.__exchange.swapToRipple()
            else:
                self.__exchangeChangeCompleted = self.__exchange.swapToUSDT()

            CryptoLogger.Logger.logDictionary({
                "Switch": state.name,
                "OrderStatus": str(self.__exchangeChangeCompleted)
            }, "StateTransition")
