#!/usr/bin/python

import sys
import time
import CryptoAnalyzer
import CryptoLogger
import CryptoServer
import threading
import time

class Analyzer:
    def startTrader(self):
        analyzer = CryptoAnalyzer.Analyzer()

        hasBalance = True
        while hasBalance:
            status = analyzer.determineAction()

            if status == CryptoAnalyzer.IterationStatus.purchaseMade:
                CryptoLogger.Logger.logIteration()
            elif status == CryptoAnalyzer.IterationStatus.terminated:
                hasBalance = False
            else:
                CryptoLogger.Logger.skipIteration()

            if hasBalance:
                time.sleep(CryptoServer.UPDATE_INTERVAL)


def startServer():
    CryptoServer.startServer()

analyzer = Analyzer()

if __name__ == '__main__':
    Analyzer.UPDATE_INTERVAL = 1
    # Kick off trader and server
    try:
        threading.Thread(target=startServer).start()
        analyzer.startTrader()
    except KeyboardInterrupt:
        sys.exit(0)
