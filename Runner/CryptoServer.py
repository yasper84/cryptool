from flask import Flask
from flask import request
from flask import Response
import json
import CryptoLogger
import cryptool
import CryptoAnalyzer
from waitress import serve
import _thread
import os

# The calling party has requires this secret in the `secret` HTTP header in order to use PUT/POST calls
__CLIENT_SECRET = "<ENTER YOUR CLIENT SECRET>"
__CALL_RESPONSE_TYPE = "application/json"

APP = Flask(__name__)

# App-Editable parameters
NUM_SUCCESSIONS_REQUIRED: int = 3
MIN_CHANGE_DELTA: float = 0.002
UPDATE_INTERVAL: int = 20
CRYPTO: str = "XRP"

# Retrieve status content
@APP.route('/status')
def getOutput(methods=['GET']):
        try:
                return __retrieveOutputContent()
        except:
                return Response(json.dumps({"Status:": "Unable to open output file"}),
                                status=500,
                                mimetype=__CALL_RESPONSE_TYPE)


def __retrieveOutputContent():
        outputFile = open(CryptoLogger.CRYPTOOL_LOG, 'r')
        outputContent: str = outputFile.read()
        return Response(outputContent,
                        status=200,
                        mimetype=__CALL_RESPONSE_TYPE)


# # Update Settings
@APP.route('/threshold/<threshold>', methods=['PUT'])
def updateThreshold(threshold):
        if __validateSecret() == False:
                return __notAuthorized()
        else:
                global MIN_CHANGE_DELTA
                MIN_CHANGE_DELTA = float(threshold)
                return Response(json.dumps({"Threshold:": threshold}),
                                status=200,
                                mimetype=__CALL_RESPONSE_TYPE)


@APP.route('/updateInterval/<updateInterval>', methods=['PUT'])
def updateInterval(updateInterval):
        if __validateSecret() == False:
                return __notAuthorized()
        else:
                global UPDATE_INTERVAL
                UPDATE_INTERVAL = int(updateInterval)
                return Response(json.dumps({"UpdateInterval:": updateInterval}),
                                status=200,
                                mimetype=__CALL_RESPONSE_TYPE)


@APP.route('/crypto/<crypto>', methods=['PUT'])
def updateCrypto(crypto):
        if __validateSecret() == False:
                return __notAuthorized()
        else:
                global CRYPTO
                CRYPTO = crypto
                return Response(json.dumps({"Crypto:": crypto}),
                                status=200,
                                mimetype=__CALL_RESPONSE_TYPE)


@APP.route('/successions/<successions>', methods=['PUT'])
def updateSuccessions(successions):
        if __validateSecret() == False:
                return __notAuthorized()
        else:
                global NUM_SUCCESSIONS_REQUIRED
                NUM_SUCCESSIONS_REQUIRED = int(successions)
                return Response(json.dumps({"Successions:": successions}),
                                status=200,
                                mimetype=__CALL_RESPONSE_TYPE)


# Terminate
# Update Settings
@APP.route('/terminate', methods=['POST'])
def terminate():
        if __validateSecret() == False:
                return __notAuthorized()
        else:
                _thread.interrupt_main()
                os._exit(0)


def __validateSecret():
        return request.headers.get('secret') == __CLIENT_SECRET


def __notAuthorized():
        return Response(json.dumps({"Status:": "Unauthorized"}),
                        status=401,
                        mimetype=__CALL_RESPONSE_TYPE)


# Start server
def startServer():
        serve(APP, host='0.0.0.0', port=5000)
